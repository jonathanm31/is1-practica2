package repository.jpa;

import org.springframework.stereotype.Repository;

import repository.PersonRepository;
import repository.SedeRepository;
import domain.Person;
import domain.Sede;

@Repository
public class JpaSedeRepository extends JpaBaseRepository<Sede, Long> implements
		SedeRepository {
}
