package repository;

import java.util.Collection;

import domain.Sede;

public interface SedeRepository extends BaseRepository<Sede, Long> {

}
