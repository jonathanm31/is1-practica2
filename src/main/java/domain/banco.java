package domain;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_banco", indexes = { @Index(columnList = "id") })
public class banco implements BaseEntity<Long> {

	@Id
	@SequenceGenerator(name = "banco_id_generator", sequenceName = "banco_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "banco_id_generator")
	private Long id;

	@Column(unique = true, nullable = false, updatable = false, length = 64)
	private String nombre;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	@OneToMany(mappedBy = "sede")
	private Collection<Sede> Sede;
	
/*
	@ManyToMany(mappedBy = "accounts")
	private Collection<Person> owners;

	@OneToMany(mappedBy = "sourceAccount", fetch = FetchType.LAZY)
	private Collection<Operation> sourceOperations;

	@OneToMany(mappedBy = "targetAccount")
	private Collection<Operation> targetOperations;

	@Column(nullable = false)
	private Date created = new Date();

	public banco() {
	}

	public banco(String number, double balance) {
		this.number = number;
		this.balance = balance;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public static banco copy(banco account) {
		if (account == null) {
			return null;
		}
		banco copy = new banco();
		copy.setBalance(account.getBalance());
		copy.setNumber(account.getNumber());
		return copy;
	}

	@Override
	public String toString() {
		return "{number: " + number + ", balance: " + balance + "}";
	}

	public Collection<Person> getOwners() {
		return owners;
	}

	public void setOwners(Collection<Person> owners) {
		this.owners = owners;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}*/

}
