package service;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import repository.SedeRepository;
import domain.Sede;

@Service
public class SedeService {

	@Autowired
	SedeRepository sedeRepository;

	@Transactional
	public void save(Sede sede) {
		if (sede.getId() == null) {
			sedeRepository.persist(sede);
		} else {
			sedeRepository.merge(sede);
		}
	}

	public Sede get(Long id) {
		return sedeRepository.find(id);
	}

	public Collection<Sede> getAll() {
		return sedeRepository.findAll();
	}
}
